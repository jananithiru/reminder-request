package com.bytes.reminderrequest;

import android.content.Context;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * Created by sagar.davasam on 1/9/16.
 */
public class RefreshContactsAsyncTask extends AsyncTask<Void, Void, Void> {

    private Context mContext;

    private View mView;

    public RefreshContactsAsyncTask(Context context, View view) {
        mContext = context;
        mView = view;
    }

    @Override
    protected Void doInBackground(Void... params) {
        new CustomContactsDB(mContext).RefreshContactsDatabase();

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        Snackbar.make(mView, "Contacts sync finished", Snackbar.LENGTH_LONG).show();
    }


}
