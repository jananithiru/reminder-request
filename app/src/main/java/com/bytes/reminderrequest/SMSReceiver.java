package com.bytes.reminderrequest;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.widget.Toast;

/**
 * Created by sagar on 5/22/15.
 */
public class SMSReceiver extends BroadcastReceiver{
    private static final String ACTION_SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    private Context mContext;
    private Intent mIntent;
    final SmsManager sms = SmsManager.getDefault();
    Reminder reminder;
    ReminderRequestDB reminderRequestDB;
    private String recipientName;
    private String fromContactNumber;
    private String reminderMessage;
    private int SMSRECEIVER_NOTIFICATION_ID = 1;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        mIntent = intent;

        StringBuilder smsSummary = new StringBuilder();
        reminderRequestDB = new ReminderRequestDB(mContext);

        //String action = intent.getAction();
        final Bundle bundle = intent.getExtras();
        Toast.makeText(mContext, "SMS RCVD", Toast.LENGTH_LONG);
        //action.equals(ACTION_SMS_RECEIVED) &&
        if (bundle != null) {
            Object[] pdus = (Object[]) bundle.get("pdus");
            SmsMessage[] msgs = new SmsMessage[pdus.length];
            for(int i=0;i<msgs.length;i++){
                msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                smsSummary.append(msgs[i].getMessageBody().toString())
                            .append("\n");
            }

            fromContactNumber = msgs[0].getOriginatingAddress();

            recipientName = ReminderUtils.getDisplayName(msgs[0].getOriginatingAddress(), context);

            if (recipientName == null) {
                recipientName = fromContactNumber;
            }

            String[] smsSummaryArray = smsSummary.toString().split("<>");

            Reminder.ReminderBuilder reminderBuilder = new Reminder.ReminderBuilder();
            if (smsSummaryArray.length == 1) {
                return;
            }
            if (smsSummaryArray.length < 3) {

                reminderMessage = smsSummary.toString().replace("\n", "");

                reminderBuilder.setReminderMessage(reminderMessage)
                        .setReminderContact(fromContactNumber)
                        .setbRead(ReminderUtils.REMINDER_MESSAGE_UNREAD)
                        .setbReceived(ReminderUtils.REMINDER_RECEIVED)
                        .setRecordTimeStamp();

            } else {
                reminderMessage = smsSummaryArray[2].replace("\n", "");
                if (reminderMessage.length() == 0) {
                    reminderMessage = "Blank Message";
                }
                reminderBuilder.setReminderContact(fromContactNumber)
                        .setReminderMessage(reminderMessage)
                        .setReminderDate(smsSummaryArray[0])
                        .setReminderTime(smsSummaryArray[1])
                        .setbRead(ReminderUtils.REMINDER_MESSAGE_UNREAD)
                        .setbReceived(ReminderUtils.REMINDER_RECEIVED)
                        .setRecordTimeStamp();
            }

            reminder = reminderBuilder.createReminder();

            notifyUser();

            Toast.makeText(mContext, "SMS received====>" + smsSummary, Toast.LENGTH_LONG);
            reminderRequestDB.insertData(reminder);
            //long returnValue = reminderRequestDB.insertData(reminder);
            /*if (returnValue < 0) {
                Log.e("DAVASAM", "========DATABASE INSERT (UNREAD) ERROR========");
            } else {
                Log.e("DAVASAM", "========DATABASE INSERT (UNREAD) SUCCESS========");
            }*/

        }

    }

    private void notifyUser() {
        Intent resultIntent = new Intent(mContext, ReceiveReminderActivity.class);

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext)
                .setSmallIcon(android.R.drawable.ic_popup_reminder)
                .setContentTitle("New Reminder")
                .setContentText(recipientName + ": " + reminderMessage)
                .setPriority(1)
                .setAutoCancel(true);

        resultIntent.putExtra(ReminderUtils.REMINDER_PARCELABLE_OBJECT, reminder);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingIntent);
        String notificationSound = getNewReminderSound(mContext);

        if (notificationSound.length() > 0) {
            mBuilder.setSound(Uri.parse(notificationSound));
        } else {
            mBuilder.setDefaults(Notification.DEFAULT_SOUND);
        }

        boolean bVibrate = PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean(ReminderPreferencesActivity.REMINDER_VIBRATE_PREFERENCE, false);

        if (bVibrate) {
            mBuilder.setVibrate(new long[]{500, 1000, 500, 1000});
        }

        notificationManager.notify(SMSRECEIVER_NOTIFICATION_ID, mBuilder.build());

    }

    private String getNewReminderSound(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String notificationSound = sharedPreferences.getString(ReminderPreferencesActivity.NEW_REMINDER_NOTIFICATION_PREFERENCE, "");
        return notificationSound;
    }

}
