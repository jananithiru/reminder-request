package com.bytes.reminderrequest;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class ReminderDialogActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_dialog);

        /*WindowManager.LayoutParams params = getWindow().getAttributes();
        params.x = -20;
        params.height = 1000;
        params.width = 1000;
        params.y = -10;

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width=dm.widthPixels;
        int height=dm.heightPixels;
        int dens=dm.densityDpi;
        double wi=(double)width/(double)dens;
        double hi=(double)height/(double)dens;

        this.getWindow().setAttributes(params);*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_reminder_dialog);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        final Intent intent = getIntent();
        final Reminder reminder = intent.getParcelableExtra(ReminderUtils.REMINDER_PARCELABLE_OBJECT);
        TextView reminderMessageTextView = (TextView)findViewById(R.id.tv_reminder_message);
        reminderMessageTextView.setText(reminder.getReminderMessage());

        TextView reminderContactTextView = (TextView) findViewById(R.id.reminder_contact_in_dialog);

        String reminderContact = reminder.getReminderContact();
        String displayName = getDisplayName(reminderContact);
        if (displayName != null && displayName.length() > 0) {
            reminderContactTextView.setText("From: " + displayName);
        } else {
            reminderContactTextView.setText("From: " + reminderContact);
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                Intent intent1 = new Intent(getApplicationContext(),ReceiveReminderActivity.class);
                intent1.putExtra(ReminderUtils.REMINDER_PARCELABLE_OBJECT, reminder);
                startActivity(intent1);

            }
        });
    }

    public String getDisplayName(String strPhoneNumber) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(strPhoneNumber));
        Cursor query = getContentResolver().query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (query.getCount() == 0) {
            return null;
        }
        query.moveToFirst();
        int displayNameColumnIndex = query.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        String displayName = query.getString(displayNameColumnIndex);
        query.close();
        return displayName;
    }

}
