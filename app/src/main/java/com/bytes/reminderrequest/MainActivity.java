package com.bytes.reminderrequest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity{
    public final static String DATETIMEFORMAT = "MM-DD-YYYY HH:MM:SS";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button reminderListButton = (Button)findViewById(R.id.button_reminder_list);
        final Intent intent = new Intent(this, ReminderListActivity.class);
        reminderListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void open_send_reminder_activity(){
        Intent intent = new Intent(this, ReceiveReminderActivity.class);
        startActivity(intent);
    }
    /*public void open_send_reminder_activity(){
        Intent intent = new Intent(this, SendReminder.class);
        startActivity(intent);
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id){
            case R.id.action_compose:
                open_send_reminder_activity();
                return true;
            case R.id.action_settings:
                return true;
        }
        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }


        return super.onOptionsItemSelected(item);
    }
}
