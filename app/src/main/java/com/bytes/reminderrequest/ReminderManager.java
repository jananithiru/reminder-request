package com.bytes.reminderrequest;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by sagar on 5/22/15.
 */
public class ReminderManager {

    private Context context;
    private AlarmManager alarmManager;
    private ReminderRequestDB reminderRequestDB;

    public ReminderManager(Context context){
        this.context = context;
        this.alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        this.reminderRequestDB = new ReminderRequestDB(context);
    }

    public void setReminder(String strReminder){
        String[] strReminderArray = strReminder.split("<>");
        Toast.makeText(context,strReminder,Toast.LENGTH_LONG).show();
        Intent intent = new Intent(context, ReminderBroadcastReceiver.class);
        intent.putExtra("ReminderDetails", strReminder);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,0,intent,PendingIntent.FLAG_ONE_SHOT);
        alarmManager.set(AlarmManager.RTC_WAKEUP,getTimeStampInCalendar(strReminderArray[1]),pendingIntent);

    }

    public boolean setReminder(Reminder receivedReminder, Reminder editedReminder) {

        try {
            //Toast.makeText(context, editedReminder.toString(), Toast.LENGTH_LONG).show();
            reminderRequestDB.updateRecord(receivedReminder, editedReminder);

            Intent intent = new Intent(context, ReminderBroadcastReceiver.class);
            intent.putExtra("ReminderDetails", editedReminder.toString());
            intent.putExtra(ReminderUtils.REMINDER_PARCELABLE_OBJECT, editedReminder);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);

            //TODO: delete old reminder when updating new reminder?
            alarmManager.set(AlarmManager.RTC_WAKEUP, getTimeStampInCalendar(editedReminder.getReminderTimeStamp()), pendingIntent);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public void deleteReminder(Reminder reminder) {

        reminderRequestDB.deleteRecord(reminder);
        Intent intent = new Intent(context, ReminderBroadcastReceiver.class);

        intent.putExtra("ReminderDetails", reminder.toString());

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        alarmManager.cancel(pendingIntent);

    }

    public static long getTimeStampInCalendar(String timeStamp){
        DateFormat dateFormat = new SimpleDateFormat(ReminderUtils.TIMESTAMP_FORMAT);
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(dateFormat.parse(timeStamp));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return calendar.getTimeInMillis();
    }

}
