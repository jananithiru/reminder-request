package com.bytes.reminderrequest;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


public class SendReminder extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    //private EditText phoneNumber;
    private TextView date;
    private TextView time;
    private EditText message;
    private static String SENT="SMS_SENT";
    private static String DELIVERED="SMS_DELIVERED";
    private static int MAX_SMS_MESSAGE_LENGTH = 128;
    private String strDate;
    private String strTime;
    private String strMessage = "";
    private String strPhoneNumber = "";
    private Button btSend;
    private Button btCancel;
    private final Context context = this;
    private String finalMessage;
    private Reminder reminder;
    private SearchView searchView;
    private int recipientTVID;
    private int colorPrimary;
    private HashMap<Integer, String> recipientMap;
    private LinearLayout linearLayout;
    private ScrollView recipientScrollView;
    private LinearLayout.LayoutParams params;
    private LinearLayout linearLayoutForRow;
    private int widthSoFar;
    private int maxWidth;
    private int linearLayoutForRowID;
    private Reminder incomingReminder;
    private ReminderRequestDB reminderRequestDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_reminder_1);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_send_reminder);
        setSupportActionBar(toolbar);

        //Snackbar.make(SendReminder.this.findViewById(R.id.activity_reminder_list_id), "Press CANCEL to exit", Snackbar.LENGTH_LONG).show();

        Toast.makeText(this, "Press CANCEL to exit", Toast.LENGTH_LONG).show();

        //initialize to random value
        linearLayoutForRowID = 2904;

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        searchView = (SearchView) findViewById(R.id.contactSearchView);
        SearchableInfo searchableInfo = searchManager.getSearchableInfo(getComponentName());
        searchView.setSearchableInfo(searchableInfo);
        searchView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    SendReminder.this.finish();

                }
                return false;
            }
        });
        searchView.setOnSuggestionListener(contactsSuggestionListener);
        searchView.setOnQueryTextListener(contactsQueryTextListener);

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {

            @Override
            public boolean onClose() {
                SendReminder.this.finish();
                return false;
            }

        });

        recipientMap = new HashMap<>();
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = context.getTheme();
        theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
        colorPrimary = typedValue.data;

        linearLayout = (LinearLayout) findViewById(R.id.recipientLinearLayout);

        recipientScrollView = (ScrollView) findViewById(R.id.recipientScrollView);

        linearLayoutForRow = new LinearLayout(context);
        linearLayoutForRow.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        //linearLayoutForRow.setGravity(Gravity.LEFT);
        linearLayoutForRow.setOrientation(LinearLayout.HORIZONTAL);

        linearLayoutForRow.setId(linearLayoutForRowID);
        linearLayout.addView(linearLayoutForRow);

        //set current date as default date
        strDate = new SimpleDateFormat(ReminderUtils.DATE_FORMAT).format(new Date());
        //set current time as default time
        strTime = new SimpleDateFormat(ReminderUtils.TIME_FORMAT).format(new Date());

        Intent intent = getIntent();
        incomingReminder = intent.getParcelableExtra(ReminderUtils.REMINDER_PARCELABLE_OBJECT);
        if (incomingReminder != null) {
            //FORWARD
            if (incomingReminder.getReminderType() == ReminderUtils.ReminderTypes.FORWARD) {

                strDate = incomingReminder.getReminderDate();
                strTime = incomingReminder.getReminderTime();
                strMessage = incomingReminder.getReminderMessage();
            } else { //REPLY
                searchView.setQuery(incomingReminder.getReminderContact(), true);
                //strPhoneNumber = incomingReminder.getReminderContact();
            }
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        maxWidth = displayMetrics.widthPixels - 40;

        date = (TextView) findViewById(R.id.tv_send_date);
        //date.setHint("Set Date");
        date.setText(ReminderUtils.getUserDisplayDate(strDate));
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog(findViewById(android.R.id.content));
            }

        });

        time = (TextView) findViewById(R.id.tv_send_time);
        time.setText(ReminderUtils.getUserDisplayTimeFormat(strTime));
        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog(findViewById(android.R.id.content));
            }
        });

        message = (EditText)findViewById(R.id.etMessage);
        if (strMessage.length() == 0) {
            message.setHint("Type your message here..");
        } else {
            message.setText(strMessage);
        }

        message.addTextChangedListener(messageTextWatcher);

        btSend = (Button)findViewById(R.id.buttonSend);
        btSend.setOnClickListener(sendButtonListener);

        btCancel = (Button)findViewById(R.id.buttonCancel);
        btCancel.setOnClickListener(cancelButtonListener);

        //removes search icon from search view
        int magId = getResources().getIdentifier("android:id/search_mag_icon", null, null);
        ImageView magImage = (ImageView) searchView.findViewById(magId);
        magImage.setLayoutParams(new LinearLayout.LayoutParams(0, 0));

    }


    SearchView.OnSuggestionListener contactsSuggestionListener = new SearchView.OnSuggestionListener() {
        @Override
        public boolean onSuggestionSelect(int position) {
            searchView.setQuery("", false);
            return true;
        }

        @Override
        public boolean onSuggestionClick(int position) {
            Cursor searchCursor = (Cursor) searchView.getSuggestionsAdapter().getItem(position);
            String contactName = searchCursor.getString(searchCursor.getColumnIndex(SearchManager.SUGGEST_COLUMN_TEXT_1));
            String contactNumber = searchCursor.getString(searchCursor.getColumnIndex(SearchManager.SUGGEST_COLUMN_TEXT_2));
            addrecipients(contactName, getTelephoneNumber(contactNumber));
            searchView.setQuery("", false);
            searchView.setQueryHint("To:");
            return true;
        }

    };

    SearchView.OnQueryTextListener contactsQueryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            strPhoneNumber = getTelephoneNumber(query);
            addrecipients(getDisplayName(strPhoneNumber), strPhoneNumber);
            searchView.setQuery("", false);
            return true;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            strPhoneNumber = getTelephoneNumber(newText);
            return true;
        }

    };

    private void addrecipients(String contactName, String contactNumber) {

        final TextView recipientTextView = new TextView(context);
        if (contactName != null) {
            System.out.println();
        }
        if (contactName != null && contactName.length() > 0) {
            recipientTextView.setText(contactName);
        } else {
            recipientTextView.setText(contactNumber);
        }
        recipientTextView.setBackgroundColor(colorPrimary);

        recipientTextView.setPadding(10, 5, 10, 5);

        recipientTextView.setTextSize(16);

        recipientTextView.measure(0, 0);

        params = new LinearLayout.LayoutParams(recipientTextView.getMeasuredWidth(), LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(10, 5, 10, 5);
        //linearLayoutForEachView.addView(recipientTextView, params);
        //linearLayoutForEachView.measure(0, 0);
        //linearLayoutForEachView.setId(recipientTVID++);
        //linearLayoutForEachView.setOnClickListener(recipientOnTouchListener);
        recipientTextView.setOnClickListener(recipientOnTouchListener);
        recipientTextView.setId(++recipientTVID);
        recipientMap.put(recipientTVID, contactNumber);

        widthSoFar += recipientTextView.getMeasuredWidth();

        if (widthSoFar >= maxWidth) {
            //linearLayout.addView(linearLayoutForRow);
            linearLayoutForRow = new LinearLayout(context);

            linearLayoutForRow.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            //linearLayoutForRow.setGravity(Gravity.LEFT);
            linearLayoutForRow.setOrientation(LinearLayout.HORIZONTAL);
            params = new LinearLayout.LayoutParams(recipientTextView.getMeasuredWidth(), recipientTextView.getMeasuredHeight());
            params.setMargins(10, 5, 10, 5);
            widthSoFar = recipientTextView.getMeasuredWidth();
            linearLayoutForRow.setId(++linearLayoutForRowID);
            linearLayoutForRow.addView(recipientTextView, params);
            linearLayout.addView(linearLayoutForRow);

        } else {
            LinearLayout tempLayoutForRow = (LinearLayout) findViewById(linearLayoutForRowID);
            linearLayout.removeView(tempLayoutForRow);
            tempLayoutForRow.addView(recipientTextView, params);
            linearLayout.addView(tempLayoutForRow);
        }

    }

    TextView.OnClickListener recipientOnTouchListener = new TextView.OnClickListener() {
        @Override
        public void onClick(final View v) {
            AlertDialog.Builder editRecipient = new AlertDialog.Builder(context);
            editRecipient.setTitle("Edit Recipient");
            editRecipient.setPositiveButton("REMOVE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    LinearLayout temp = (LinearLayout) v.getParent();
                    temp.removeView(v);
                    recipientMap.remove(v.getId());
                    if (temp.getChildCount() == 0) {
                        LinearLayout parent = (LinearLayout) temp.getParent();
                        if (parent.getChildCount() > 1) {
                            parent.removeView(temp);
                        } else {
                            linearLayoutForRowID = temp.getId();
                        }

                    }

                }
            });
            editRecipient.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            editRecipient.create().show();
        }
    };


    @Override
    protected void onNewIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            addrecipients("", getTelephoneNumber(query));
        }
    }

    private String getTelephoneNumber(String searchText) {

        StringBuilder result = new StringBuilder();

        if (searchText.length() > 0 && searchText.charAt(0) == '+') {
            result.append("+");
            searchText = searchText.substring(1);
        }

        for (char c : searchText.toLowerCase().toCharArray()) {
            switch (c) {
                case '0':
                    result.append("0");
                    break;
                case '1':
                    result.append("1");
                    break;
                case '2':
                case 'a':
                case 'b':
                case 'c':
                    result.append("2");
                    break;
                case '3':
                case 'd':
                case 'e':
                case 'f':
                    result.append("3");
                    break;
                case '4':
                case 'g':
                case 'h':
                case 'i':
                    result.append("4");
                    break;
                case '5':
                case 'j':
                case 'k':
                case 'l':
                    result.append("5");
                    break;
                case '6':
                case 'm':
                case 'n':
                case 'o':
                    result.append("6");
                    break;
                case '7':
                case 'p':
                case 'q':
                case 'r':
                case 's':
                    result.append("7");
                    break;
                case '8':
                case 't':
                case 'u':
                case 'v':
                    result.append("8");
                    break;
                case '9':
                case 'w':
                case 'x':
                case 'y':
                case 'z':
                    result.append("9");
                    break;

            }
        }

        return result.toString();
    }

    public static void hideSoftKeyBoard(FragmentActivity activity){
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(FragmentActivity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);

    }
    private View.OnClickListener cancelButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Discard Message")
                    .setMessage("Are you sure?")
                    .setPositiveButton("Discard", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SendReminder.this.finish();
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
    };

    private View.OnClickListener sendButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finalMessage = new String(strDate + "<>" + strTime + "<>" + strMessage);

            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            /*Log.e("DAVASAM", "Network type: "+telephonyManager.getNetworkType());
            Log.e("DAVASAM", "Phone type: "+telephonyManager.getPhoneType());
            Log.e("DAVASAM", "SIM state: "+telephonyManager.getSimState());*/

            if (telephonyManager.getSimState() == telephonyManager.SIM_STATE_ABSENT || telephonyManager.getSimState() == telephonyManager.SIM_STATE_UNKNOWN) {
                Snackbar.make(v, "Phone is not connected to carrier. Please check.", Snackbar.LENGTH_LONG).show();
                return;
            }

            if (recipientMap.size() == 0 && strPhoneNumber.length() == 0) {
                Snackbar.make(v, "No contact added", Snackbar.LENGTH_LONG).show();
                return;
            }

            if (strPhoneNumber.length() > 0) {
                recipientMap.put(recipientMap.size() + 1, strPhoneNumber);
            }

            Reminder.ReminderBuilder reminderBuilder = new Reminder.ReminderBuilder()
                    .setReminderDate(strDate)
                    .setReminderTime(strTime)
                    .setReminderMessage(strMessage)
                    .setbReceived(ReminderUtils.REMINDER_SENT)
                    .setbRead(ReminderUtils.REMINDER_MESSAGE_READ);


            new SendSMSAsync(recipientMap, finalMessage, reminderBuilder, context).execute();

            SendReminder.this.finish();
        }
    };

    private TextWatcher messageTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            strMessage = s.toString();
        }
    };


    public static class DatePickerFragment extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker

            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), (SendReminder)getActivity(), year, month, day);
        }

    }
    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();

        newFragment.show(getSupportFragmentManager(), "datePicker");

    }
    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Do something with the date chosen by the user
        strDate = month+1 + "-" + day + "-" + year;
        date.setText(ReminderUtils.getUserDisplayDate(month + 1 + "-" + day + "-" + year));
    }

    public static class TimePickerFragment extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), (SendReminder)getActivity(), hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }


    }
    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    //@Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        strTime = hourOfDay + ":" + minute;
        time.setText(ReminderUtils.getUserDisplayTimeFormat(hourOfDay + ":" + minute));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent keyEvent) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            this.finish();
        }
        return true;
    }

    @Override
    public void onBackPressed(){
        //super.onBackPressed();

        if (!searchView.isIconified()) {
            searchView.setIconified(true);
        }

        if(checkLengthsSendReminder()){
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Discard Reminder")
                    .setMessage("Are you sure?")
                    .setPositiveButton("Discard", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SendReminder.this.finish();
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
        else{
            SendReminder.this.finish();
        }

    }

    public boolean checkLengthsSendReminder(){
        if (strMessage.length() > 0 || recipientMap.size() > 0) {
            return true;
        }
        return false;
    }

    public String getDisplayName(String strPhoneNumber) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(strPhoneNumber));
        Cursor query = getContentResolver().query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (query.getCount() == 0) {
            return null;
        }
        query.moveToFirst();
        int displayNameColumnIndex = query.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        String displayName = query.getString(displayNameColumnIndex);
        query.close();
        return displayName;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_send_reminder, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_refresh_contacts_1:
                new RefreshContactsAsyncTask(context, findViewById(R.id.activity_send_reminder_1)).execute();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSearchRequested() {
        return super.onSearchRequested();
    }

}
