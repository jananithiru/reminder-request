package com.bytes.reminderrequest;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.ViewGroup;

/**
 * Created by sagar.davasam on 12/5/15.
 */
public class HomeFragmentPagerAdapter extends FragmentStatePagerAdapter implements ViewPager.OnPageChangeListener {

    final int PAGE_COUNT = 2;
    private String tabTitles[] = new String[]{"UPCOMING", "ALL"};
    private Context mcontext;
    private static FutureRemindersFragment futureRemindersFragment;
    private static AllRemindersFragment allRemindersFragment;

    public HomeFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public HomeFragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mcontext = context;
    }

    @Override
    public Fragment getItem(int position) {

        if (position == 0) {
            futureRemindersFragment = FutureRemindersFragment.newInstance(position + 1);
            return futureRemindersFragment;
        } else if (position == 1) {
            allRemindersFragment = AllRemindersFragment.newInstance(position + 1);
            return allRemindersFragment;
        }
        return null;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        return super.instantiateItem(container, position);
    }

    public static Fragment getFragment(int position) {
        if (position == 0) {
            return futureRemindersFragment;
        } else if (position == 1) {
            return allRemindersFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

   /* @Override
    public int getItemPosition(Object object) {
        notifyDataSetChanged();
        return POSITION_NONE;
    }*/

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        getItem(position).onResume();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
