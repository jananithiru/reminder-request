package com.bytes.reminderrequest;

import android.content.Context;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * Created by sagar.davasam on 12/1/15.
 */
public class ListViewGestureListener extends GestureDetector.SimpleOnGestureListener {

    Context mContext;
    ImageView deleteButton;
    RelativeLayout listviewLayout;

    public ListViewGestureListener(Context context, View view){
        mContext = context;
        listviewLayout = (RelativeLayout)view.findViewById(R.id.layout_front);
        deleteButton = (ImageView)view.findViewById(R.id.deleteImage);

    }

    @Override
    public boolean onFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {
        float diffX = me2.getX() - me1.getX();
        float diffY = me2.getY() - me1.getY();
        deleteButton.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });
        if (Math.abs(diffX) > Math.abs(diffX) && Math.abs(diffX) > 50) {
            if(diffX<0){
                Log.v("DAVASAM", "Swipe Right to Left");
                if (deleteButton.getVisibility() != View.GONE) {
                    deleteButton.setVisibility(View.GONE);
                }
            }else{
                Log.v("DAVASAM", "Swipe Left to Right");
                if (deleteButton.getVisibility() == View.GONE) {
                    deleteButton.setVisibility(View.VISIBLE);
                }
            }
        }
        return true;
    }

    private View.OnTouchListener deleteButtonListener = new View.OnTouchListener(){

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return false;
        }
    };


}
