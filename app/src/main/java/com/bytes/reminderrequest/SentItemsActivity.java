package com.bytes.reminderrequest;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

public class SentItemsActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private CustomAdapter mAdapter;

    private Context mContext = this;

    private static final int SENT_ITEMS_LOADER_ID = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sent_items);

        Toolbar sentItemsActivityToolBar = (Toolbar) findViewById(R.id.sent_items_toolbar);
        setSupportActionBar(sentItemsActivityToolBar);

        ListView sentItemsListView = (ListView) findViewById(R.id.sent_items_listview);

        mAdapter = new CustomAdapter(mContext, new ReminderRequestDB(mContext).getSentItems(), 0);

        sentItemsListView.setAdapter(mAdapter);

        sentItemsListView.setOnItemClickListener(new ListItemClickListener(mContext, mAdapter, 0));

        getLoaderManager().initLoader(SENT_ITEMS_LOADER_ID, null, this);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        mAdapter.swapCursor(new ReminderRequestDB(mContext).getSentItems());
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new Loader<>(mContext);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(new ReminderRequestDB(mContext).getSentItems());
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }
}
