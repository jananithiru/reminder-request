package com.bytes.reminderrequest;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by sagar.davasam on 11/28/15.
 */
public class CustomAdapter extends CursorAdapter {

    private LayoutInflater mLayoutInflater;
    private Context mContext;
    Reminder reminder;
    ReminderRequestDB reminderRequestDB;
    ReminderManager reminderManager;
    Cursor mCursor;
    SimpleDateFormat simpleDateFormat;

    public CustomAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        reminderRequestDB = new ReminderRequestDB(context);
        reminderManager = new ReminderManager(context);
        mCursor = c;
        simpleDateFormat = new SimpleDateFormat(ReminderUtils.TIMESTAMP_FORMAT);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = mLayoutInflater.inflate(R.layout.custom_row_layout_1, parent, false);
        return view;
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        final String reminderMessage = cursor.getString(2);
        final String reminderTimeStamp = simpleDateFormat.format(new Date(cursor.getLong(3)));
        final String reminderDate = ReminderUtils.getUserDisplayDate(reminderTimeStamp.split(" ")[0]);
        final String reminderTime = ReminderUtils.getUserDisplayTimeFormat(reminderTimeStamp.split(" ")[1]);
        final String reminderContact = cursor.getString(1);
        if (cursor.getColumnCount() == 5) {
            final String reminderIsRead = cursor.getString(4);

            UnreadImageDrawableView unreadCustomView = (UnreadImageDrawableView) view.findViewById(R.id.custom_view_unread_indicator);
            if (Integer.parseInt(reminderIsRead) == ReminderUtils.REMINDER_MESSAGE_UNREAD) {
                unreadCustomView.setVisibility(View.VISIBLE);
            } else {
                unreadCustomView.setVisibility(View.GONE);
            }
        }

        TextView messageTextView = (TextView) view.findViewById(R.id.messageText1);
        messageTextView.setText(reminderMessage);

        TextView timeStampTextView = (TextView) view.findViewById(R.id.timeStampText1);
        timeStampTextView.setText(reminderTime);

        TextView dateTextView = (TextView) view.findViewById(R.id.dateText_row_layout);
        dateTextView.setText(reminderDate);

        TextView contactNameTextView = (TextView) view.findViewById(R.id.contactName);

        String displayName = ReminderUtils.getDisplayName(reminderContact, mContext);

        if (displayName != null && displayName.length() > 0) {
            contactNameTextView.setText(displayName);
        } else {
            contactNameTextView.setText(reminderContact);

        }
    }

}
