package com.bytes.reminderrequest;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.telephony.SmsManager;

import java.util.HashMap;

/**
 * Created by sagar on 5/22/15.
 */
public class SendSMSAsync extends AsyncTask {

    private String message;

    private HashMap<Integer, String> recipientMap;

    private Reminder.ReminderBuilder reminderBuilder;

    private Context context;

    public static String SENT = "com.bytes.reminderRequest.SMS_SENT";
    public static String DELIVERED = "com.bytes.reminderRequest.SMS_DELIVERED";
    public static String CONTACT_DISPLAY_NAME = "contact_display_name";

    public SendSMSAsync(HashMap<Integer, String> recipientMap, String message, Reminder.ReminderBuilder reminderBuilder, Context context) {
        this.recipientMap = recipientMap;
        this.message = message;
        this.reminderBuilder = reminderBuilder;
        this.context = context;
    }

    @Override
    protected Object doInBackground(Object[] params) {

        if (recipientMap != null && recipientMap.size() > 0) {
            for (Integer key : recipientMap.keySet()) {

                reminderBuilder.setReminderContact(recipientMap.get(key));
                sendSMS(recipientMap.get(key), message);
                new ReminderRequestDB(context).insertData(reminderBuilder.createReminder());

            }
        }

        return null;
    }

    private void sendSMS(String phoneNumber, String message) {
        if (phoneNumber.length() == 0 || phoneNumber == null) {
            return;
        }
        if (phoneNumber.length() > 10) {
            phoneNumber = phoneNumber.substring(phoneNumber.length() - 10);
        }

        String displayName = ReminderUtils.getDisplayName(phoneNumber, context);
        if (displayName == null || displayName.length() == 0) {
            displayName = phoneNumber;
        }

        PendingIntent piSent = PendingIntent.getBroadcast(context, 0, new Intent(SENT).putExtra(CONTACT_DISPLAY_NAME, displayName), 0);
        PendingIntent piDelivered = PendingIntent.getBroadcast(context, 0, new Intent(DELIVERED).putExtra(CONTACT_DISPLAY_NAME, displayName), 0);

        //context.registerReceiver(new SMSStatusReceiver(SENT, phoneNumber, context), new IntentFilter(SENT));
        //context.registerReceiver(new SMSStatusReceiver(DELIVERED, phoneNumber, context), new IntentFilter(DELIVERED));

        SmsManager smsManager = SmsManager.getDefault();

        smsManager.sendTextMessage(phoneNumber, null, message, piSent, piDelivered);

    }

}
