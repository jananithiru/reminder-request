package com.bytes.reminderrequest;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Created by sagar.davasam on 12/6/15.
 */
public class ReminderDialogFragment extends DialogFragment {

    public static ReminderDialogFragment newInstance() {
        ReminderDialogFragment reminderDialogFragment = new ReminderDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", "Reminder Details");
        reminderDialogFragment.setArguments(args);
        return reminderDialogFragment;

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        return new AlertDialog.Builder(getActivity())

                .setTitle(getArguments().getString("title"))
                .setPositiveButton("EDIT",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                            }
                        }
                )
                .setNegativeButton("DELETE",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                            }
                        }
                )
                .create();
    }
}
