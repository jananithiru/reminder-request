package com.bytes.reminderrequest;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by sagar.davasam on 1/16/16.
 */
public class ListItemClickListener implements ListView.OnItemClickListener {

    private Context mContext;

    private Reminder reminder;

    private ReminderManager reminderManager;

    private int mTab;

    private CursorAdapter mAdapter;

    private Fragment mFragment;

    private SimpleDateFormat simpleDateFormat;

    public ListItemClickListener(Context context, CursorAdapter cursorAdapter, int tab, Fragment fragment) {
        mContext = context;
        reminderManager = new ReminderManager(context);
        mTab = tab;
        mAdapter = cursorAdapter;
        mFragment = fragment;
        simpleDateFormat = new SimpleDateFormat(ReminderUtils.TIMESTAMP_FORMAT);
    }
    public ListItemClickListener(Context context, CursorAdapter cursorAdapter, int tab) {
        mContext = context;
        reminderManager = new ReminderManager(context);
        mTab = tab;
        mAdapter = cursorAdapter;
        mFragment = null;
        simpleDateFormat = new SimpleDateFormat(ReminderUtils.TIMESTAMP_FORMAT);
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        String reminderMessage = ((Cursor) mAdapter.getItem(position)).getString(2);
        String reminderTimeStamp = simpleDateFormat.format(new Date(((Cursor) mAdapter.getItem(position)).getLong(3)));
        String reminderDate = ReminderUtils.getUserDisplayDate(reminderTimeStamp.split(" ")[0]);
        String reminderTime = ReminderUtils.getUserDisplayTimeFormat(reminderTimeStamp.split(" ")[1]);
        String reminderContact = ((Cursor) mAdapter.getItem(position)).getString(1);

        Reminder.ReminderBuilder reminderBuilder = new Reminder.ReminderBuilder();
        reminderBuilder.setReminderMessage(reminderMessage)
                .setReminderContact(reminderContact)
                .setReminderTimeStamp(reminderTimeStamp);

        reminder = reminderBuilder.createReminder();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext, R.style.MyAlertDialogStyle);
        alertDialogBuilder.setTitle("Reminder Details");

        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View alertDialogView = layoutInflater.inflate(R.layout.alert_dialog_layout, null);
        TextView reminderMessageAlert = (TextView) alertDialogView.findViewById(R.id.reminder_message_alert_dialog);
        TextView reminderTimeAlert = (TextView) alertDialogView.findViewById(R.id.reminder_timestamp_alert_dialog);
        TextView reminderDateAlert = (TextView) alertDialogView.findViewById(R.id.reminder_date_alert_dialog);
        TextView reminderContactName = (TextView) alertDialogView.findViewById(R.id.reminder_contact_name_alert_dialog);

        reminderMessageAlert.setText(reminderMessage);
        reminderTimeAlert.setText(reminderTime);
        reminderDateAlert.setText(reminderDate);

        String contactName = ReminderUtils.getDisplayName(reminderContact, mContext);
        if (contactName != null && contactName.length() > 0) {
            reminderContactName.setText(contactName);
        } else {
            reminderContactName.setText(reminderContact);
        }

        alertDialogBuilder.setView(alertDialogView);
        //alertDialogBuilder.setMessage(reminder.toString());
        alertDialogBuilder.setPositiveButton("EDIT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(mContext, ReceiveReminderActivity.class);
                intent.putExtra(ReminderUtils.REMINDER_PARCELABLE_OBJECT, reminder);
                mContext.startActivity(intent);
            }
        });

        alertDialogBuilder.setNegativeButton("DELETE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //reminderRequestDB.deleteRecord(reminder);
                reminderManager.deleteReminder(reminder);
                //mAdapter.notifyDataSetChanged();
                if (mTab >= 1) {

                    mAdapter.swapCursor(mTab == 1 ? new ReminderRequestDB(mContext).getFutureRecords() : new ReminderRequestDB(mContext).getAllRecords());

                } else {
                    mAdapter.swapCursor(new ReminderRequestDB(mContext).getSentItems());
                }

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }
}
