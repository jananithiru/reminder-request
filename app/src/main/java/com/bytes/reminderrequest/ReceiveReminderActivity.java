package com.bytes.reminderrequest;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by sagar on 5/22/15.
 */
public class ReceiveReminderActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener{

    private TextView dateET;
    private TextView timeET;
    private EditText messageET;
    private TextView fromPhnNumET;
    String reminderDate = "";
    String reminderTime = "";
    String reminderMessage = "";
    String strPhoneNumber = "";
    private Button addButton;
    private Button discardButton;
    final Context context = this;
    ReminderManager reminderManager;
    Reminder receivedReminder;
    Reminder editedReminder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rcv_reminder_1);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_receive_reminder);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        if (intent.hasExtra(ReminderUtils.REMINDER_PARCELABLE_OBJECT)) {
            receivedReminder = intent.getParcelableExtra(ReminderUtils.REMINDER_PARCELABLE_OBJECT);
            strPhoneNumber = receivedReminder.getReminderContact();
            reminderDate = receivedReminder.getReminderDate();
            reminderTime = receivedReminder.getReminderTime();
            reminderMessage = receivedReminder.getReminderMessage();
        } else {
            String smsString = "4694123890<>11/11<>11:11<>Eleven eleven";
            String[] smsStringArray = smsString.split("<>");
            strPhoneNumber = smsStringArray[0];
            reminderDate = (new SimpleDateFormat(ReminderUtils.DATE_FORMAT)).format(new Date());
            reminderTime = (new SimpleDateFormat(ReminderUtils.TIME_FORMAT)).format(new Date());
            reminderMessage = smsStringArray[3];

        }
        Reminder.ReminderBuilder reminderBuilder = new Reminder.ReminderBuilder()
                .setReminderContact(strPhoneNumber)
                .setReminderDate(reminderDate)
                .setReminderTime(reminderTime)
                .setReminderMessage(reminderMessage);

        //.setbRead(ReminderUtils.REMINDER_MESSAGE_READ);//comment when SendReminder activity is linked to Compose buttong

        receivedReminder = reminderBuilder.createReminder();

        reminderManager = new ReminderManager(context);

        fromPhnNumET = (TextView) findViewById(R.id.tv_rcv_contact);
        String displayName = getDisplayName(strPhoneNumber);
        if (displayName != null && displayName.length() > 0) {
            fromPhnNumET.setText(displayName);
        } else {
            fromPhnNumET.setText(strPhoneNumber);
        }

        messageET = (EditText)findViewById(R.id.et_rcv_message);
        messageET.setText(reminderMessage);
        messageET.addTextChangedListener(messageTextWatcher);

        dateET = (TextView) findViewById(R.id.tv_rcv_date);
        dateET.setText(reminderDate);
        dateET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialogRcvReminder(findViewById(android.R.id.content));
            }
        });

        timeET = (TextView) findViewById(R.id.tv_rcv_time);
        timeET.setText(reminderTime);
        timeET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialogRcvReminder(findViewById(android.R.id.content));
            }
        });

        discardButton = (Button)findViewById(R.id.discard_reminder_button);
        discardButton.setOnClickListener(discardButtonListener);

        addButton = (Button)findViewById(R.id.add_reminder_button);
        addButton.setOnClickListener(addButtonListener);

    }

    private TextWatcher messageTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            reminderMessage = s.toString();
        }
    };
    public static class DatePickerFragment extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), (ReceiveReminderActivity)getActivity(), year, month, day);
        }

    }
    public void showDatePickerDialogRcvReminder(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");

    }

    public static class TimePickerFragment extends DialogFragment
    {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), (ReceiveReminderActivity)getActivity(), hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

    }
    public void showTimePickerDialogRcvReminder(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        //dateET.setText(month+1 + "-" + day + "-" + year);
        reminderDate = month+1 + "-" + day + "-" + year;
        dateET.setText(ReminderUtils.getUserDisplayDate(reminderDate));
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        reminderTime = hourOfDay + ":" + minute;
        timeET.setText(ReminderUtils.getUserDisplayTimeFormat(hourOfDay + ":" + minute));
    }

    private View.OnClickListener discardButtonListener = new View.OnClickListener(){

        @Override
        public void onClick(View view) {
            showDiscardDialog();
        }
    };

    private View.OnClickListener addButtonListener = new View.OnClickListener(){

        @Override
        public void onClick(View view) {

            Reminder.ReminderBuilder reminderBuilder = new Reminder.ReminderBuilder();
            reminderBuilder.setReminderMessage(reminderMessage)
                    .setReminderTimeStamp(reminderDate, reminderTime)
                    .setReminderContact(strPhoneNumber)
                    .setbReceived(ReminderUtils.REMINDER_RECEIVED)
                    .setbRead(ReminderUtils.REMINDER_MESSAGE_READ);

            editedReminder = reminderBuilder.createReminder();
            if (reminderManager.setReminder(receivedReminder, editedReminder)) {
                Snackbar.make(view, "Reminder added", Snackbar.LENGTH_LONG).show();
            } else {
                Snackbar.make(view, "Error in adding reminder. Please try again", Snackbar.LENGTH_LONG).show();
            }
            //Toast.makeText(context, "Reminder added", Toast.LENGTH_LONG).show();

            ReceiveReminderActivity.this.finish();
        }
    };

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        showDiscardDialog();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_receive_reminder, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_reply:
                Toast.makeText(context, "REPLY", Toast.LENGTH_LONG).show();
                Reminder.ReminderBuilder reminderBuilderReply = new Reminder.ReminderBuilder();
                reminderBuilderReply.setReminderContact(strPhoneNumber)
                        .setReminderType(ReminderUtils.ReminderTypes.REPLY);
                editedReminder = reminderBuilderReply.createReminder();

                Intent replyIntent = new Intent(context, SendReminder.class);
                replyIntent.putExtra(ReminderUtils.REMINDER_PARCELABLE_OBJECT, editedReminder);
                startActivity(replyIntent);
                ReceiveReminderActivity.this.finish();
                return true;
            case R.id.action_forward:
                Toast.makeText(context, "FORWARD", Toast.LENGTH_LONG).show();

                Reminder.ReminderBuilder reminderBuilderForward = new Reminder.ReminderBuilder();
                reminderBuilderForward.setReminderMessage(reminderMessage)
                        .setReminderTimeStamp(reminderDate, reminderTime)
                        .setReminderContact(strPhoneNumber)
                        .setbReceived(ReminderUtils.REMINDER_RECEIVED)
                        .setbRead(ReminderUtils.REMINDER_MESSAGE_READ)
                        .setReminderType(ReminderUtils.ReminderTypes.FORWARD);

                editedReminder = reminderBuilderForward.createReminder();

                Intent forwardIntent = new Intent(context, SendReminder.class);
                forwardIntent.putExtra(ReminderUtils.INTENT_REPLY_OR_FORWARD, ReminderUtils.INTENT_FORWARD);
                forwardIntent.putExtra(ReminderUtils.REMINDER_PARCELABLE_OBJECT, editedReminder);
                startActivity(forwardIntent);
                ReceiveReminderActivity.this.finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public String getDisplayName(String strPhoneNumber) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(strPhoneNumber));
        Cursor query = getContentResolver().query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (query.getCount() == 0) {
            return null;
        }
        query.moveToFirst();
        int displayNameColumnIndex = query.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        String displayName = query.getString(displayNameColumnIndex);
        query.close();
        return displayName;
    }

    public void showDiscardDialog(){
        if(checkLengthsRcvReminder()){
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Discard Reminder")
                    .setMessage("Are you sure?")
                    .setPositiveButton("Discard", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //TODO: check http://stackoverflow.com/questions/6413700/android-proper-way-to-use-onbackpressed
                            //delete received reminder record from database
                            new ReminderRequestDB(context).deleteUnreadRecord(receivedReminder);
                            ReceiveReminderActivity.this.finish();
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
        else{
            ReceiveReminderActivity.this.finish();
        }
    }
    public boolean checkLengthsRcvReminder(){
        if (reminderMessage.length() > 0 || strPhoneNumber.length() > 0) {
            return true;
        }
        return false;

    }

}
