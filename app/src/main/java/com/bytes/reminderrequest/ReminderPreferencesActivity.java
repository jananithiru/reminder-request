package com.bytes.reminderrequest;

import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;


/**
 * Created by sagar.davasam on 12/12/15.
 */
public class ReminderPreferencesActivity extends AppCompatActivity {

    public static String NEW_REMINDER_NOTIFICATION_PREFERENCE = "new_reminder_notification";

    public static String REMINDER_RINGTONE_PREFERENCE = "reminder_ringtone";

    public static String REMINDER_VIBRATE_PREFERENCE = "reminder_vibrate";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preferences_layout);
        Toolbar preferencesToolBar = (Toolbar) findViewById(R.id.preferences_toolbar);
        setSupportActionBar(preferencesToolBar);

        getFragmentManager().beginTransaction().replace(R.id.frame_layout_preferences, new ReminderPreferenceFragment()).commit();
    }


    private static Preference.OnPreferenceChangeListener preferenceSummaryListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            String summaryValue = newValue.toString();

            if (TextUtils.isEmpty(summaryValue)) {
                preference.setSummary("");
            } else {
                Ringtone ringtone = RingtoneManager.getRingtone(preference.getContext(), Uri.parse(summaryValue));
                if (ringtone == null) {
                    preference.setSummary(null);
                } else {
                    String ringtoneName = ringtone.getTitle(preference.getContext());
                    preference.setSummary(ringtoneName);
                }
            }
            return true;
        }
    };

    public static class ReminderPreferenceFragment extends PreferenceFragment {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.reminder_pref);

            bindPreferenceValueToListener(findPreference(NEW_REMINDER_NOTIFICATION_PREFERENCE));
            bindPreferenceValueToListener(findPreference(REMINDER_RINGTONE_PREFERENCE));
            bindPreferenceValueToListener(findPreference(REMINDER_VIBRATE_PREFERENCE));

        }
    }

    private static void bindPreferenceValueToListener(Preference preference) {
        preference.setOnPreferenceChangeListener(preferenceSummaryListener);
        if (preference.getTitle().equals("Vibrate")) {
            preferenceSummaryListener.onPreferenceChange(preference, PreferenceManager.
                    getDefaultSharedPreferences(preference.getContext()).
                    getBoolean(preference.getKey(), false));
        } else {
            preferenceSummaryListener.onPreferenceChange(preference, PreferenceManager.
                    getDefaultSharedPreferences(preference.getContext()).
                    getString(preference.getKey(), ""));
        }

    }

}
