package com.bytes.reminderrequest;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by sagar.davasam on 12/7/15.
 */
public final class ReminderUtils {

    private ReminderUtils() {

    }

    public static final int REMINDER_MESSAGE_READ = 1;

    public static final int REMINDER_MESSAGE_UNREAD = 0;

    public static final int REMINDER_SENT = 0;

    public static final int REMINDER_RECEIVED = 1;

    public static final String REMINDER_PARCELABLE_OBJECT = "reminderObject";

    public static final String TIMESTAMP_FORMAT = "MM-dd-yyyy HH:mm";

    public static final String DATE_FORMAT = "MM-dd-yyyy";

    public static final String TIME_FORMAT = "HH:mm";

    public static final String USER_DISPLAY_DATE_FORMAT = "EEE, MMM d, yyyy";

    public static final String USER_DISPLAY_TIME_FORMAT = "h:mm a";

    public static final String TAG = "ReminderRequestLog";

    public static final String INTENT_REPLY = "Intent_Reply";

    public static final String INTENT_FORWARD = "Intent_Forward";

    public static final String INTENT_REPLY_OR_FORWARD = "Reply_Or_Forward";

    public static String formatPhoneNumber(String phoneNumber) {

        String formattedPhoneNumber = phoneNumber;


        return formattedPhoneNumber;
    }

    public static class ReminderTypes {
        public static final int DEFAULT = 0;
        public static final int REPLY = 1;
        public static final int FORWARD = 2;

    }

    public static String getDisplayName(String strPhoneNumber, Context context) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(strPhoneNumber));
        Cursor query = context.getContentResolver().query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (query.getCount() == 0) {
            return null;
        }
        query.moveToFirst();
        int displayNameColumnIndex = query.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        String displayName = query.getString(displayNameColumnIndex);
        query.close();
        return displayName;
    }

    public static long getEpoch(String timeStamp) {
        long epoch = new Date().getTime();
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(TIMESTAMP_FORMAT);
            epoch = simpleDateFormat.parse(timeStamp).getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return epoch;
    }

    public static String getUserDisplayDate(String inputDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(USER_DISPLAY_DATE_FORMAT);
        String userDisplayDate = "";
        Date inputDateObject = new Date();
        Calendar inputCalendarObject = Calendar.getInstance();
        Calendar nowCalendarObject = Calendar.getInstance();
        try {
            userDisplayDate = simpleDateFormat.format(new SimpleDateFormat(DATE_FORMAT).parse(inputDate));
            inputDateObject = new SimpleDateFormat(DATE_FORMAT).parse(inputDate);
            inputCalendarObject.setTime(inputDateObject);
            if (inputCalendarObject.get(Calendar.DATE) == nowCalendarObject.get(Calendar.DATE)) {
                return "Today";
            } else if (inputCalendarObject.get(Calendar.DATE) - nowCalendarObject.get(Calendar.DATE) == 1) {
                return "Tomorrow";
            } else if (nowCalendarObject.get(Calendar.DATE) - inputCalendarObject.get(Calendar.DATE) == 1) {
                return "Yesterday";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userDisplayDate;
    }

    public static String getUserDisplayTimeFormat(String inputTime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(USER_DISPLAY_TIME_FORMAT);
        String userDisplayTime = "";
        try {
            userDisplayTime = simpleDateFormat.format(new SimpleDateFormat(TIME_FORMAT).parse(inputTime));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userDisplayTime;
    }
}
